from freeswitch import *
from freepybx.model.pbx import PbxEndpoint
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
engine = create_engine('postgresql://freepybx:secretpass456@127.0.0.1/freepybx')

Session = sessionmaker(bind=engine)
db = Session()

class FreePyBX:

    def __init__(self, session):
        self.session = session
        self.session.setHangupHook(self.hangup_hook)

    def hangup_hook(self):
        del self.session
        return

    def main(self):
        try:
            is_forward = True \
                if self.session.getVariable("is_forward") == 'true' else False
            endpoint = db.query(PbxEndpoint). \
                filter(auth_id==self.session.getVariable("username"),
                       domain==self.session.getVariable("domain")). \
                fetchone()
            if endpoint:
                if is_forward:
                    endpoint.find_me = True
                    endpoint.follow_me_1 = \
                        self.session.getVariable("forward_number")[:]
                else:
                    endpoint.find_me = False
            consoleLog("error", "No endpoint for: {0} at {1}".format(
                self.session.getVariable("username"),
                self.session.getVariable("domain")))
        except Exception, e:
            consoleLog("error",
                       "Excepted in call_forward.py: {0}".format(e) + "\n")
        finally:
            del self.session
            return

def handler(session, *args):
    freepybx = FreePyBX(session)
    freepybx.main()
    freepybx.hangup_hook()
